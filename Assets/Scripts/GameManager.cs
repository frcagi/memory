﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{

    //TextosUI
    public Text textoPuntos;
    public Text textGameOver;
    private List<Vector3> posicionesCarta = new List<Vector3>();
    //52 Cartas (sprite)    
    public List<Sprite> cartasSprite = new List<Sprite>();


    //Will find all Components which will have the tag "card"
    public GameObject[] cartasObject;
    public CardScript[] cartasJuego;//Cartas para mostrar
    //Array for Saving the actual positions (index) of both carts which are being shown
    private int[] cardsUp = new int[2];

    //Guardamos los CardScript de los GameObjects del escenario


    //Pref

    private List<CardScript> scripts; //Guardamos los CardScript de los GameObjects del escenario    
    private string state = "inicial";//Estado inicial

    public GameObject prefCarta;
    //Variables 
    private int nParejasLogradas = 0;
    private float xPosition, yPosition;
    private IEnumerator coroutine;
    // Start is called before the first frame update
    private void Awake()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 1; j <= 2; j++)
            {
                posicionesCarta.Add(new Vector3(i, j, 0));
            }
        }
        //Saco posiciones para 
        xPosition = prefCarta.GetComponent<Transform>().position.x;
        yPosition = prefCarta.GetComponent<Transform>().position.y;
        for (int i = 0; i < posicionesCarta.Count; i++)
        {
            Instantiate(prefCarta, posicionesCarta[i], Quaternion.identity);

        }
        cartasObject = GameObject.FindGameObjectsWithTag("card");
        scripts = new List<CardScript>();

        for (int i = 0; i < 10; i++)
        {
            scripts.Add(cartasObject[i].GetComponent<CardScript>());
        }

        for (int i = 0; i < 5; i++)
        {

            int nCarta = Random.Range(0, cartasSprite.Count);
            scripts[i].front = cartasSprite[nCarta];
            scripts[i + 5].front = cartasSprite[nCarta];
            scripts[i].value = nCarta;
            scripts[i + 5].value = nCarta;
        }
        cartasJuego = new CardScript[scripts.Count];
        Shuffle();

        textGameOver.text = "";

    }
    void Start()
    {

        cardsUp[0] = -1;
        cardsUp[1] = -1;
    }
    // Update is called once per frame
    void Update()
    {

        textoPuntos.text = "Has encontrado " + nParejasLogradas + " parejas. ";

    }

    public void MoveState(int index)
    {

        switch (state.ToLower())
        {
            case "inicial":
                cardsUp[0] = index;
                state = "carta_descubierta";
                break;
            case "carta_descubierta":
                cardsUp[1] = index;
                if ((cartasJuego[cardsUp[0]].value == cartasJuego[cardsUp[1]].value) &&
                    (cartasJuego[cardsUp[0]].Index != cartasJuego[cardsUp[1]].Index))
                {
                    nParejasLogradas++;

                    if (nParejasLogradas == 5)
                    {
                        textGameOver.text = "¡Has ganado!";
                        StartCoroutine("waittokill");
                    }
                    cartasObject[cardsUp[0]].SetActive(false);
                    cartasObject[cardsUp[1]].SetActive(false);
                    Debug.Log("Voy a eliminar el index " + cardsUp[0] + " " + cardsUp[1]);
                }
                else
                {

                    cartasJuego[cardsUp[0]].StartCoroutine("esperar2sec");
                    cartasJuego[cardsUp[1]].StartCoroutine("esperar2sec");
                }
                cardsUp[0] = -1;
                cardsUp[1] = -1;
                state = "inicial";
                break;
        }

    }
    IEnumerator waittokill()
    {

        yield return new WaitForSeconds(3f);
        Application.Quit();
    }


    void Shuffle()
    {

        List<GameObject> lista_auxiliar = new List<GameObject>();
        for (int i = 0; i < cartasJuego.Length; i++)
        {
            lista_auxiliar.Add(cartasObject[i]);
        }
        for (int i = 0; i < cartasJuego.Length; i++)
        {
            int aleatoria = Random.Range(0, posicionesCarta.Count);
            int nRandom = Random.Range(0, scripts.Count);
            cartasObject[i] = lista_auxiliar[nRandom];
            cartasObject[i].transform.position = posicionesCarta[aleatoria];
            posicionesCarta.RemoveAt(aleatoria);
            cartasJuego[i] = scripts[nRandom];
            cartasJuego[i].Index = i;
            Debug.Log(cartasJuego[i].value);
            scripts.RemoveAt(nRandom);
            lista_auxiliar.RemoveAt(nRandom);
        }
    }


}
