﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour
{
    public Sprite front;
    public Sprite back;
    public int value;
    private SpriteRenderer spriteRender;
    private bool faceUp;
    public int Index;   
   private GameManager gm;

    private void Awake()
    {
        spriteRender = GetComponent<SpriteRenderer>();
        gm = GameObject.FindWithTag("gameManager").GetComponent<GameManager>();

    }
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame

    private void OnMouseDown()
    {
        Toggle();
        gm.MoveState(Index);

    }
    //Cambiamos las caras de las cartas
    public void Toggle()
    {
    
        if (!faceUp)
        {
            faceUp = true;            
            spriteRender.sprite = front;
        }
       else
       {
            faceUp = false;
            spriteRender.sprite = back;
        }
    }
    //Esperamos 2 segundos para volver a girar las cartas si no son correctas.
    public IEnumerator esperar2sec()
    {
        yield return new WaitForSeconds(1f);
        Toggle();
    }
}
